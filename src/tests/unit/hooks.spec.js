
import CookieBanner from '@/components/CookieBanner.vue'
describe('CookieBanner.vue', () => {
  it('a le hook `data`', () => {
    expect(typeof CookieBanner.data).toBe('function')
  });
  it('a le hook `computed`', () => {
    expect(typeof CookieBanner.computed).toBe('object')
  });
  it('a le hook `methods`', () => {
    expect(typeof CookieBanner.methods).toBe('object')
  })
});
