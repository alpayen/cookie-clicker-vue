# cookie-clicker-vue
A simple cookie clicker app with no purpose other than learn simple devOps tools.

## Pipeline logic
The project is pushed to GitLab which then handles CI/CD logic.  
GitLab deploys the apps on Heroku (preprod / prod)  
4 stages : build, tests, deploy-preprod, deploy  
  
`build` and `tests` are run on both `dev` and `master` branch.  
`deploy-preprod` in exclusive to the `dev` branch  
`deploy` in exclusive to the `master` branch  

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your unit tests
```
npm run test:unit
```
